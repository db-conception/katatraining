﻿using Gestion_BU.Entities;

namespace Gestion_BU.Services
{
    public class ForfaitService : IForfaitService
    {
        public int DetermineNbTelechargementMaximum(Forfait forfait)
        {
            if (forfait == Forfait.Standard)
            {
                return 10;
            }
            else if (forfait == Forfait.Premium)
            {
                return 10 * 2;
            }

            return 0;
        }
    }
}
