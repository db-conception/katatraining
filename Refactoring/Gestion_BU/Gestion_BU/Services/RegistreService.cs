﻿using Gestion_BU.Entities;
using Gestion_BU.Repositories;

namespace Gestion_BU.Services
{
    public class RegistreService
    {
        private IForfaitService forfaitService;
        private EdutiantRepository etudiantRepository;
        private UniversiteRepository universiteRepository;
        public RegistreService(IForfaitService _forfaitService,
            EdutiantRepository _etudiantRepository, UniversiteRepository _universiteRepository
            )
        {
            this.forfaitService = _forfaitService;
            this.etudiantRepository = _etudiantRepository;
            this.universiteRepository = _universiteRepository;
        }
        public bool AjouterEtudiant(string emailAddress, int universityId)
        {
            Console.WriteLine(string.Format("Log: Debut Ajout d'un etudiant avec cet e-mail '{0}'", emailAddress));

            if (etudiantRepository.Exists(emailAddress))
            {
                return false;
            }

            var universite = universiteRepository.GetById(universityId);

            var currEtudiant = new Etudiant(emailAddress, universityId);

            currEtudiant.NbTelechargementMaximum = forfaitService.DetermineNbTelechargementMaximum(universite.Forfait);

            etudiantRepository.Add(currEtudiant);
            
            Console.WriteLine(string.Format("Log: Fin Ajout d'un etudiant avec cet e-mail '{0}'", emailAddress));

            return true;
        }
    }
}
