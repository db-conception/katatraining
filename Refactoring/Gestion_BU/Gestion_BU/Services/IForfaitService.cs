﻿using Gestion_BU.Entities;

namespace Gestion_BU.Services
{
    public interface IForfaitService
    {
        public int DetermineNbTelechargementMaximum(Forfait forfait);
    }
}
